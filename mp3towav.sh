#!/usr/bin/env bash
# Fecha: 2017.06.11
# Convierte pistas mp3 a wav.
#
# AJUSTES
# Codec de audio (-acodec) a usar, listar disponibles con:
## avconv -formats | grep -i pcm
#
# Ratio de audio:
## Sample Rates Quality (-ar option)
## 44100  -Audio CD Quality
## 37800  -CD-XA Audio
## 22050  -One half the sampling rate of Audio CD
## 16000  -Low Quality
#
error=0
DIR="/home/$USER/Escritorio"

mkdir -p "${DIR}/wav"

if [[ -e /usr/bin/ffmpeg ]]; then
    PROBE='ffmpeg'
elif [[ -e /usr/bin/avconv ]]; then
    PROBE='avconv'
else
    echo "Se necesita tener ffmpeg o avconv instalado."
    exit 1
fi

# Si las pistas estan dentro de subdirectorios
if [[ $(find ./* -maxdepth 0 -type d) ]]; then
    for dir in *; do
        DIR="${DIR}/wav/${dir}_wav"
        mkdir -p "$DIR"
        for file in "${dir}"/*.mp3; do
            echo "Convirtiendo la pista $file ..."
            track="${file##*/}"
            "$PROBE" -i "${file}" -acodec pcm_s16le -ar 22050 "${DIR}/${track/.mp3/.wav}" 2> /dev/null
            error=$((error + $?))
        done
    done
else
    # Si las pistas se encuentran en el directorio actual
    DIR="${DIR}/wav/${PWD##*/}"
    mkdir -p "$DIR"
    for file in *.mp3; do
        echo "Convirtiendo la pista $file ..."
        track="${file##*/}"
        "$PROBE" -i "${file}" -acodec pcm_s16le -ar 22050 "${DIR}/${track/.mp3/.wav}" 2> /dev/null
        error=$((error + $?))
    done
fi

[[ $error == 0 ]] && echo "Conversion finalizada." || echo "Algo no ha ido bien."
