#!/usr/bin/env bash
# Fecha: 2018.05.12
# Extrae los fotogramas de un vídeo y elimina las bandas negras

readonly OUTDIR="${HOME}/Escritorio/photos"
readonly FPS=15        # Toma 4 frames cada segundo

VIDEO="$1"    # Archivo de vídeo
TITULO="$2"   # AGREGAR el título sin espacios de las fotos


# Sin argumentos o con más de dos, muestra mensaje de ayuda
if [[ $# -eq 0 ]] || [[ $# -gt 2 ]]; then
    echo "Número de argumentos incorrecto."
    echo "Uso:"
    echo "  $0 <video> [titulo_salida]"
    exit 1
fi

mkdir -p "${OUTDIR}/tmp"

echo "Extrayendo fotogramas del vídeo..."

ffmpeg -ss 00:00:02 -i "${VIDEO}" \
    -loglevel quiet \
    -vf fps="${FPS}"/60 \
    -f image2 "${OUTDIR}/tmp/${TITULO:-photo}"_%03d.jpg

NUM_IMG=$(find "${OUTDIR}"/tmp/*.jpg | wc -l)

echo "${NUM_IMG} fotogramas extraidos..."; sleep 1
echo "Eliminando bandas negras..."; sleep 1

find "${OUTDIR}/tmp" -maxdepth 1 -name '*.jpg' \
| while read -r foto; do
    nombre="${foto##*/}"
    convert -trim "$foto" "${OUTDIR}/${nombre}" 2> /dev/null
done

echo "Eliminando archivos temporales..."; sleep 1
# Toma las portadas del vídeo originales
mv "${OUTDIR}/tmp/${TITULO:-photo}"_00{1..2}.jpg "${OUTDIR}/"
rm -r "${OUTDIR}/tmp"

echo "Tarea finalizada."

