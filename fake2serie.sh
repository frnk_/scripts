#!/bin/bash
# Date: 2018.10.24
## Replica el directorio actual de series con sus archivos en '/tmp/lab'
## Si se da un archivo con capitulos, replica estos en '/tmp/lab'.

ifsbak=$IFS
IFS=$'\n'

[[ ! -d "/tmp/lab" ]] && mkdir -p "/tmp/lab"

_help() {
    echo "Uso:
    -Crear replica del directorio de series actual.
      "${0##*/}"
    -Crear replica desde un archivo.
      "${0##*/}" [archivo_de_capitulos]"

    exit
}

_all_dirs() {
    _bucle() {
        for files in $(ls "${seriedir}/${capdir##*/}"); do
            touch "$exitdir/${capdir##*/}/$files"
        done
    }

    local exitdir="/tmp/lab/${PWD##*/}"
    local seriedir="$PWD"

    mkdir -p "$exitdir" && echo "Directorio creado en $exitdir"

    # Comprueba si existen directorios o solo archivos
    if ls ./*/ 1> /dev/null 2>&1; then
        for capdir in $seriedir/*; do
            mkdir -p "${exitdir}/${capdir##*/}"
            _bucle
        done
    else
        _bucle
    fi
}

_read_list() {
    read -p "Nombre de la serie/directorio: " titulo
    mkdir -p "/tmp/lab/${titulo}"
    while read line; do
        touch "/tmp/lab/${titulo}/${line}"
    done < "$1"
    
    echo "Archivos creados en /tmp/lab/$titulo"
}

### MAIN ###

[[ $1 == "-h" || $1 == "--help" ]] && _help

echo "Creando archivos falsos en /tmp/lab/ ..."
[[ $# == 0 ]] && _all_dirs
[[ $# == 1 ]] && _read_list "$1"

IFS=$ifsbak
