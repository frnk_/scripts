#!/usr/bin/env bash
# Date: 2017.09.25
# Lista la duracion de todas las peliculas/capítulos en el directorio.

IFS=$'\n'
LONGITUD=0
LIST=($(find . -maxdepth 1 -type f -regextype egrep -regex '.*\.(avi|mkv|mp4)$' | sort))

if [[ -e /usr/bin/ffprobe ]]; then
    PROBE='ffprobe'
elif [[ -e /usr/bin/avprobe ]]; then
    PROBE='avprobe'
else
    echo "Se necesita tener ffmpeg o avconv instalado."
    exit 1
fi

# Obtiene el valor de la lista mas larga (para dar formato a printf)
for i in "${LIST[@]##*/}"; do
    l="${#i}"
    if [[ "$l" -gt "$LONGITUD" ]]; then
        LONGITUD="$l"
    fi
done

LONGITUD=$((LONGITUD + 4))

_salida() {
    for j in "${LIST[@]##*/}"; do
        t=$("$PROBE" "$j" 2>&1 | awk '/Duration/ {print $1" "$2}')
        printf "%-${LONGITUD}s %s\n" "$j" "${t/,/}"
    done
}

# Ordena la salida por duración de menor a mayor
_salida | sort -t: -k2 -k3 -k4

