#!/usr/bin/env bash
# Gira todos los vídeos del directorio 90 grados a la derecha.
# Para girar 180 grados: (transpose=1,transpose=1).

IFS=$'\n'
count=1

for video in *.mp4; do
	avconv -i "$video" -vf transpose=1 -crf 19 -strict experimental \
	"${video/.mp4/}_${count}.mp4"
	((count++))
done

