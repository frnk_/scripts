#!/usr/bin/env bash

# Date: 2019.05.26
# Toma los nombres de archivos de peliculas de una lista y los busca desde el
# directorio actual. Copia los que coinciden en el directorio que indiquemos.
# Guarda en el escritorio un archivo log con información del resultado.

IFS=$'\n'
DATE=$(date +%F)
declare -i COUNT=0
declare -r INPUT_LIST=($(<"${1:-$(help)}"))  # Archivo de entrada con los títulos a buscar
declare -r OUTPUT_DIR="$2"     # Directorio donde copiar los títulos
declare -a FOUND
declare -a NOT_FOUND
declare -a LOG_EXIST
declare -a LOG_COPIED
declare -r LOG_FILE="${HOME}/Escritorio/copify_log_${DATE}"

help() {
    cat <<EOF
Busca los títulos de LISTA en el directorio actual y subdirectorios,
luego copia los que coinciden en el DIRECTORIO indicado.
  Uso: "${0##*/}" 'LISTA' 'DIRECTORIO'
EOF
} >&2

search_files() {
    local title
    while IFS= read -r; do
        title=$(find . -iname "$REPLY")
        if [[ -n "$title" ]]; then
            FOUND=("${FOUND[@]}" "$title")
        else
            NOT_FOUND=("${NOT_FOUND[@]}" "$REPLY")
        fi
    done <<< "${INPUT_LIST[*]}"
}

copy_files() {
    local output_dir
    local media_title
    local media

    output_dir="${OUTPUT_DIR%/}"

    for media in "${FOUND[@]}"; do
        media_title="${media##*/}"

        if [[ -f "${output_dir}/${media}" ]]; then
            echo "[-] ${media_title} ya existe..."
            LOG_EXIST=(${LOG_EXIST[@]} "${media_title}")
        else
            pv -N "[+] ${media_title}" "$media" > "${output_dir}/${media_title}"
            LOG_COPIED=(${LOG_COPIED[@]} "${media_title}")
            ((COUNT++))
        fi
    done
}


## MAIN ##

[[ ! $(which pv) ]] && echo "Dependencia incumplida: pv" >&2 && exit 1

[[ "${#@}" -ne 2 ]] && \
    echo "[!] Deben indicarse 2 argumentos." >&2 && help && exit 2

search_files

[[ "${#FOUND[@]}" -eq 0 ]] && \
    echo "No se ha encontrado ninguna coincidencia." >&2 && exit 3

echo -e "Copiando archivos...\n"
printf "Archivos en la lista:  %02d\n"  "${#INPUT_LIST[@]}"
printf "Archivos encontrados:  %02d\n\n"  "${#FOUND[@]}"

copy_files

printf "\nArchivos copiados:     %02d\n" "$COUNT"
printf "Duración:           %02d:%02d\n" "$((SECONDS/60))" "$((SECONDS%60))"

echo -e "## Archivos no encontrados ##\n${NOT_FOUND[*]}" > "$LOG_FILE"
echo -e "\n## Archivos copiados ##\n${LOG_COPIED[*]}" >> "$LOG_FILE"
echo -e "\n## Archivos omitidos ##\n${LOG_EXIST[*]}" >> "$LOG_FILE"
echo -e "\nLog guardado en el Escritorio."
