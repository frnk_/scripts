#!/usr/bin/env bash

# Date: 2019.03.05
# Crea el archivo ICON y un enlace simbólico a una imagen SVG necesarios
# para personalizar los iconos de la extensión NAUTILUS-EMBLEMS.

EMBLEMS_PATH="${HOME}/.icons/hicolor/48x48/emblems"
EMBLEMS_SVG_PATH="${HOME}/Imágenes/emblems"
EMBLEM="$1"  # Archivo imagen
# Si $2 es nulo, toma como valor el nombre de la imagen sin extensión
EMBLEM_NAME="${2:-${EMBLEM/.svg/}}"  # Nombre archivo sin extensión
EMBLEM_NAME_DEST=""
EMBLEM_FILE_TEXT="
[Icon Data]

DisplayName=$EMBLEM_NAME
DisplayName[en_GB]=$EMBLEM_NAME
DisplayName[es]=$EMBLEM_NAME"


help() {
    cat <<EOF
Modo de empleo: ${0##*/} IMAGEN.svg [NOMBRE]

Crea el archivo ICON y un enlace simbólico a una imagen SVG necesarios para
personalizar los iconos de la extensión NAUTILUS-EMBLEMS.
NOMBRE corresponde al nombre que se asigna al emblema, si no se especifica
toma el nombre de la imagen SVG.
EOF
}

[[ "$1" == '-h' || "$1" == '--help' ]] && help && exit

# Comprueba si esta instalado nautilus-emblems
[ ! -f /usr/share/nautilus-python/extensions/emblems.py ] && \
    echo "NAUTILUS-EMBLEMS no se encuentra instaldo" 1>&2 && exit 1

[[ $(mimetype -b "$EMBLEM") != 'image/svg+xml' ]] && \
    echo "$EMBLEM no es una imagen SVG" 1>&2 && exit 1

# Crea el directorio EMBLEMS_PATH si no existe
[ ! -d "$EMBLEMS_PATH" ] && mkdir -p "$EMBLEMS_PATH" && \
    echo "Creado el directorio ${EMBLEMS_PATH}..."

# Crea el directorio EMBLEMS_SVG_PATH si no existe
[ ! -d "$EMBLEMS_SVG_PATH" ] && mkdir -p "$EMBLEMS_SVG_PATH" && \
    echo "Creado el directorio ${EMBLEMS_SVG_PATH}..."

EMBLEM_NAME="${EMBLEM_NAME}.svg"
EMBLEM_NAME_DEST="${EMBLEMS_SVG_PATH}/${EMBLEM_NAME}"

# Si EMBLEM_NAME.svg existe en EMBLEMS_SVG_PATH
# (Evita sobreescribir imagenes distintas con el mismo nombre)
if [[ -f "$EMBLEM_NAME_DEST" ]]; then
    # Si el directorio actual no es EMBLEMS_SVG_PATH
    if [ "$PWD" != "$EMBLEMS_SVG_PATH" ]; then
        echo "Encontrado un archivo con el mismo nombre..."
        echo "Renombrando $EMBLEM_NAME a ${RANDOM}-${EMBLEM_NAME}..."
        mv "$EMBLEM_NAME_DEST" "${EMBLEMS_SVG_PATH}/${RANDOM}-${EMBLEM_NAME}"
        echo "Copiando ${EMBLEM_NAME} a ${EMBLEMS_SVG_PATH}..."
        cp "$EMBLEM" "$EMBLEM_NAME_DEST"
    fi
elif [ "$PWD" = "$EMBLEMS_SVG_PATH" ]; then
    true
else
    echo "Copiando $EMBLEM a ${EMBLEMS_SVG_PATH}..."
    echo "Renombrando $EMBLEM a ${EMBLEM_NAME}..."
    cp "$EMBLEM" "$EMBLEM_NAME_DEST"
fi

(
    cd "$EMBLEMS_PATH" || exit 2
    # Si existe un enlace con el mismo nombre lo elimina
    if [ -L "emblem-${EMBLEM_NAME}" ]; then
        echo "Encontrado enlace con el mismo nombre..."
        rm "emblem-${EMBLEM_NAME}"
        echo "Eliminado enlace a emblem-${EMBLEM_NAME}..."
    fi
        
    echo "Creando enlace simbólico emblem-${EMBLEM_NAME}..."
    ln -s "${EMBLEMS_SVG_PATH}/${EMBLEM_NAME}" "emblem-${EMBLEM_NAME}"
    # Modifica la extensión svg a icon
    echo "Creando archivo emblem-${EMBLEM_NAME/svg/icon}..."
    echo "$EMBLEM_FILE_TEXT" > "emblem-${EMBLEM_NAME/svg/icon}"
)

echo "Reiniciar Nautilus para que los cambios tengan efecto [s/N]?"
while true; do
    read -n1 -r -p "> "
    case $REPLY in
        s|S) nautilus -q; break ;;
        n|N) echo ''; break ;;
        * )  echo ''
             echo "$REPLY no es una opción valida, introduce [s] o [n]" ;;
    esac
done

echo -e "\nTarea finalizada."

