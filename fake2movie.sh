#!/usr/bin/env bash

# Fecha: 2019.05.25
# A partir de una LISTA con títulos de peliculas, crea
# archivos vacios con estos en /tmp/lab/movies

LISTA="$1"
COUNT=0

_help() {
    cat <<EOF
Uso: "${0##*/}" LISTA
EOF
}

_crea_fake() {
	while read MOVIE; do
		touch /tmp/lab/movies/"${MOVIE}"
		((COUNT++))
	done < "$LISTA" 2> /dev/null
}

[[ ! -d /tmp/lab/movies ]] && mkdir -p /tmp/lab/movies

if [[ -z "$1" ]]; then
	echo "Error, no se ha indicado ningún parámetro."; _help
	exit 1
elif [[ $# -ne 1 ]]; then
	echo "Error, se ha indicado más de un parámetro."; _help
	exit 2
elif [[ ! -e "$1" ]]; then
	echo "Error, no existe el archivo indicado"; _help
	exit 3
elif [[ ! -s "$1" ]]; then
    echo "El archivo está vacio."; _help
    exit 4
else
	echo "Parámetro correcto."
fi

_crea_fake

[[ $COUNT -gt 0 ]] \
    && echo "$COUNT títulos creados en /tmp/lab/movies/." \
    || echo "No se ha creado ningún título."

