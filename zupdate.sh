#!/usr/bin/env bash

# Version: 2018.02.22
# Autor: Francisco Pellicer
# Descomentar las líneas de las distribuciones que se desee activar.

CODENAME='bionic'
#DISTRO=(ubuntu kubuntu lubuntu lubuntu-next edubuntu ubuntu-budgie ubuntu-mate ubuntukylin ubuntu-server neon)
# ARCH='i386'     # Descomentar para i386 (excepto Ubuntu, Ubuntu server y Neon)
PS3='Opción: '

# Comprueba que zsync este instalado y da la opción de instalar
if [[ ! $(which zsync) ]]; then
    echo "El comando 'zsync' no se encuentra instaldo, puede hacer este paso"
    echo "por su cuenta o escribir 'aceptar' (sin comillas) para proseguir."

    read -p "Realizar la instalación: " OPCION
    if [[ "${OPCION}" == 'aceptar' ]]; then
        ( sudo apt install zsync -y ) &> /dev/null && echo "Instalado correctamente." \
        || echo "ERROR en la instalción." >&2
        exit
    else
        "Cancelado." >&2
        exit
    fi
fi

# Listado de urls a los archivos zsync
link[0]="http://cdimage.ubuntu.com/ubuntu/daily-live/current/${CODENAME}-desktop-amd64.iso.zsync"
link[1]="http://cdimage.ubuntu.com/kubuntu/daily-live/current/${CODENAME}-desktop-${ARCH:-amd64}.iso.zsync"
# link[2]="http://cdimage.ubuntu.com/xubuntu/daily-live/current/${CODENAME}-desktop-${ARCH:-amd64}.iso.zsync"
# link[3]="http://cdimage.ubuntu.com/lubuntu/daily-live/current/${CODENAME}-desktop-${ARCH:-amd64}.iso.zsync"
# link[4]="http://cdimage.ubuntu.com/lubuntu-next/daily-live/current/${CODENAME}-desktop-${ARCH:-amd64}.iso.zsync"
# link[5]="http://cdimage.ubuntu.com/edubuntu/daily-live/current/${CODENAME}-desktop-${ARCH:-amd64}.iso.zsync"
# link[6]="http://cdimage.ubuntu.com/ubuntu-budgie/daily-live/current/${CODENAME}-desktop-${ARCH:-amd64}.iso.zsync"
# link[7]="http://cdimage.ubuntu.com/ubuntu-mate/daily-live/current/${CODENAME}-desktop-${ARCH:-amd64}.iso.zsync"
# link[8]="http://cdimage.ubuntu.com/ubuntukylin/daily-live/current/${CODENAME}-desktop-${ARCH:-amd64}.iso.zsync"
# link[9]="http://cdimage.ubuntu.com/ubuntu-server/daily-live/current/${CODENAME}-desktop-amd64.iso.zsync"
# link[10]="http://files.kde.org/neon/images/neon-useredition/current/neon-useredition-current.iso.zsync"

_help() {
    cat <<EOF
Modo de empleo: "$0" [OPCIÓN]
Sin opciones, actualiza todas las imagenes activadas (Ubuntu por defecto).
Opciones:
  --help, -h;  Muestra esta ayuda y sale.
  --list, -l;  Lista las distribuciones activadas para actualizar.
EOF
}

_list() {
    for DISTROLINK in "${link[@]}"; do
        awk -F/ '{printf("> %s ", $4)}' <<<"${DISTROLINK}"
    done
    echo ""
}

_cambia_dir() {
    # Descarga en dir. independientes para evitar que se sobreescriban las imagenes
    [[ ! -d ./"${DISTRO}-daily" ]] && \
        mkdir ./"${DISTRO}-daily" && echo "Directorio ${DISTRO}-daily creado."
    cd ./"${DISTRO}-daily" && echo "Cambiando al directorio ${DISTRO}-daily" || exit 2
}

_actualizar() {
    echo "Se revisarán actualizaciones para las siguintes distribuciones ..."
    _list
    echo "Ctrl+C para cancelar ..."
    sleep 4
    echo "actualizando ..."

    for DISTROLINK in "${link[@]}"; do
        DISTRO=$(awk -F/ '{printf("%s", $4)}' <<<"${DISTROLINK}")
        (
        _cambia_dir

        if [[ "$DISTRO" == neon ]]; then
            # Obtiene la url del mirror de la redirección del enlace
            zsync "$(curl -w "%{url_effective}\n" -I -L -s -S "${link[10]}" -o /dev/null)"
        else
            zsync "$DISTROLINK"
        fi
        )
    done
    echo "Finalizado."
}


##### MAIN #####

if [[ "$1" == '--help' ]] || [[ "$1" == '-h' ]]; then
    _help
elif [[ "$1" == '--list' ]] || [[ "$1" == '-l' ]]; then
    _list
elif [[ "$*" -eq 0 ]]; then
    _actualizar
else
    echo "$* no es una opción válida." >&2
    exit
fi
